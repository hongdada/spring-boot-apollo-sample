package com.qhong.springboot;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(scanBasePackages = {"com.qhong.springboot"})
public class Application {
	public static void main(String[] args) throws Exception {
		ApplicationContext context = new SpringApplicationBuilder(Application.class).run(args);
		JavaConfigSample javaConfigSample = context.getBean(JavaConfigSample.class);
     	ConfigurationPropertiesSample configurationPropertiesSample=context.getBean(ConfigurationPropertiesSample.class);
		DataSourceConfig dataSourceConfig=context.getBean(DataSourceConfig.class);
		System.out.println("Application Demo. Input any key except quit to print the values. Input quit to exit.");
		while (true) {
			System.out.print("> ");
			String input = new BufferedReader(new InputStreamReader(System.in, Charsets.UTF_8)).readLine();
			if (!Strings.isNullOrEmpty(input) && input.trim().equalsIgnoreCase("quit")) {
				System.exit(0);
			}

			if(javaConfigSample!=null){
				System.out.println(javaConfigSample.toString());
			}
			if(configurationPropertiesSample!=null){
				System.out.println(configurationPropertiesSample.toString());
			}
	        if(dataSourceConfig!=null){
				System.out.println(dataSourceConfig);
			}

		}
	}
}
